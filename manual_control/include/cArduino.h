//https://github.com/ranma1988/cArduino
//Copyright (C) Dawid Urbański
//GNU GENERAL PUBLIC LICENSE Version 2
#ifndef cArduino_H
#define cArduino_H 1

/*
Arduino Serial Port Comunication

Arduino information:
	http://playground.arduino.cc/Interfacing/LinuxTTY

	warning setting up port speed does not work :/ you nead to do in mannualy:

		stty -F /dev/ttyUSB0 cs8 115200 ignbrk -brkint -icrnl -imaxbel -opost -onlcr -isig -icanon -iexten -echo -echoe -echok -echoctl -echoke noflsh -ixon -crtscts

Serial Conection Based On:
	Gary Frerking   gary@frerking.org
	http://www.tldp.org/HOWTO/Serial-Programming-HOWTO/x115.html
*/

//#include </usr/src/linux-headers-4.15.0-43/include/uapi/asm-generic/termbits.h>
#include <string>
#include <termios.h>

using namespace std;

enum ArduinoBaundRate {
  B300bps = B300,
  B600bps = B600,
  B1200bps = B1200,
  B2400bps = B2400,
  B4800bps = B4800,
  B9600bps = B9600,
  //B14400bps= B14400,
  B19200bps = B19200,
  //B28800bps = B28800,
  B38400bps = B38400,
  B57600bps = B57600,
  //B115200bps = 115200,
  ///(c) Pablo Fernández Robledo
  B115200bps = B115200,
  B1000000bps = B1000000,
  B4000000bps = B4000000
  ///(c) Pablo Fernández Robledo
};

class cArduino {

private:
  /*serial port FileDescriptor*/
  int fd = 0;

  /*memory of port settings*/
  struct termios oldtio;

  /*Arduino FileName*/
  char *MODEMDEVICE = 0;

public:
  cArduino();
  cArduino(ArduinoBaundRate baundRate);
  cArduino(ArduinoBaundRate baundRate, char *deviceFileName);

  ~cArduino();

  /*get Arduino Device FileName*/
  char *getDeviceName();
  /*Find Arduino device*/
  char *findArduino();

  /*is Arduino serial port Open?*/
  bool isOpen();

  /*open serial port, find device*/
  bool open(ArduinoBaundRate baundRate);

  /*open serial port*/
  bool open(ArduinoBaundRate baundRate, char *DeviceFileName);

  /*zamykanie*/
  void close();

  /*Flush port*/
  void flush();

  /*read from Arduino*/
  string read();

  /*read form arduino (witch timeout)
	 *ret - responce
	 *timeOut_MicroSec - (mikro sekundy 10-6)
	 *print_error - print errors to stderr?
	*/
  bool read(
      string &ret,
      unsigned long int timeOut_MicroSec = (1 * 1000000), //1sec
      bool print_error = false);

  /*
	odczytuj az do napotkania znaku / lub przekroczenia czasu
	 *ret - responce
	 *ultin - do jakiego znaku czytac
	 *timeOut_MicroSec - (mikro sekundy 10-6)
	*/
  bool read(
      string &ret,
      char until,
      unsigned long int timeOut_MicroSec);

  /*write to Arduinio*/
  void write(string text);
  ssize_t write(char *data, int length); /// (c)Pablo Fernández Robledo
};

#endif
