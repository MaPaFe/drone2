#include "communicator.h"
#include <Simulator.h>
#include <chrono>
#include <cmath>
#include <gainput/gainput.h>
#include <iostream>
#include <thread>

enum Button {
  JoyLeftX,
  JoyLeftY,
  JoyRightX,
  JoyRightY
};
int main(int argc, char* argv[]) {
  std::cout << "dfsdf" << std::endl;
  Communicator com;
  gainput::InputManager manager;
  const gainput::DeviceId padId = manager.CreateDevice<gainput::InputDevicePad>();
  gainput::InputMap map(manager);
  // axis work: min<----->center<----->max
  //                      center, max);// min is equal -max
  map.MapFloat(JoyLeftX, padId, gainput::PadButtonLeftStickX, 0, 127.5f);   // yaw
  map.MapFloat(JoyLeftY, padId, gainput::PadButtonLeftStickY, 0, -127.5f);  // throttle
  map.MapFloat(JoyRightX, padId, gainput::PadButtonAxis4, 0, 127.5f);       // roll
  map.MapFloat(JoyRightY, padId, gainput::PadButtonRightStickY, 0, 127.5f); // pitch
  int count = 0;
  while (true) {
    manager.Update();
    //if (map.GetBoolWasDown(JoyLeftX)) {
    int throttle = std::round(map.GetFloat(JoyLeftY) + 127.5);
    int pitch = std::round(map.GetFloat(JoyRightY) + 127.5);
    int roll = std::round(map.GetFloat(JoyRightX) + 127.5);
    int yaw = std::round(map.GetFloat(JoyLeftX) + 127.5);
    std::cout << "throttle: " << throttle << ", "
              << "pitch: " << pitch << ", "
              << "roll: " << roll << ", "
              << "yaw: " << yaw << std::endl;
    com.send_payload(0, throttle, pitch, roll, yaw);
    //std::cout << count % 255 << ", " << x << std::endl;
    count++;
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
    //}
  }

  return 0;
}
