#include "communicator.h"
#include <bitset>
#include <chrono>
#include <cmath>
#include <thread>

Communicator::Communicator() : arduino(baud) {
  if (!arduino.isOpen()) {
    std::cerr << "Communicator::constructor can't open arduino " << std::endl;
    arduinoOpen = false;
    return;
  }
  std::cout << "Communicator::constructor arduino open at " << arduino.getDeviceName() << "    " << std::endl;
  arduinoOpen = true;
}

Communicator::~Communicator() {}

void Communicator::send_payload(int channel, double throttle, double pitch, double roll, double yaw) { //range [-127,127]
  if (!arduinoOpen) {
    std::cerr << "Communicator::send_payload Arduino not opened, trying to open " << std::endl;
    Communicator::disconect();
    Communicator::conect();
  } else {
    throttle = Sim::Utils::constrain(std::round(throttle), -127, 127);
    pitch = Sim::Utils::constrain(std::round(pitch), -127, 127);
    roll = Sim::Utils::constrain(std::round(roll), -127, 127);
    yaw = Sim::Utils::constrain(std::round(yaw), -127, 127);
    char payload[6] = {static_cast<const char>(channel),
                       static_cast<const char>(throttle + 127),
                       static_cast<const char>(pitch + 127),
                       static_cast<const char>(roll + 127),
                       static_cast<const char>(yaw + 127),
                       '\0'}; //needs an
                              // const char c[2] = {static_cast<const char>(channel),
                              //                    '\0'};
    //auto t1_r = std::chrono::high_resolution_clock::now(); // latency test
    if (arduino.write(payload, 6) == -1) {
      std::cerr << "Communicator::send_payload failed to write to Arduino, trying to disconect and conect  " << std::endl;
      Communicator::disconect();
      Communicator::conect();
    } else {
      arduino.read();
    }
    //auto t2_r = std::chrono::high_resolution_clock::now();                                                        // latency test
    //std::cout << "t:" << std::chrono::duration_cast<std::chrono::microseconds>(t2_r - t1_r).count() << std::endl; // latency test
  }
}

void Communicator::disconect() {
  arduino.close();
}
bool Communicator::isOpen() {
  if (!arduino.isOpen()) {
    std::cerr << "Communicator::isOpen can't open Arduino " << std::endl;
    arduinoOpen = false;
    return false;
  } else {
    std::cout << "Communicator::isOpen Arduino open at " << arduino.getDeviceName() << "     " << std::endl;
    arduinoOpen = true;
    return true;
  }
}
void Communicator::conect() {
  if (!arduino.open(baud)) {
    std::cerr << "Communicator::conect can't open arduino" << std::endl;
    arduinoOpen = false;
    return;
  }
  std::cout << "Communicator::conect arduino open at " << arduino.getDeviceName() << std::endl;
  arduinoOpen = true;
}
