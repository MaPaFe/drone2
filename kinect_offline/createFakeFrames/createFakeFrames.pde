// This programs generates sinthetic kinect frames(float array) and saves them as a bynary file(recording_0.kinrec) inside the data folder
// Simply draw with Processing, keeping into acount, that brightness[0,255] is interpreted as distance between 0mm and 5000mm; BLACK(0) is 0mm
// Then copy recording_0.kinrec to the same directory as the kinect_read executable
//(c) Pablo Fernández Robledo 2019

import java.io.*;
import java.nio.ByteBuffer;

int frame = 0;

void setup() {
  size(512, 424);
  rectMode(CENTER);
}

void draw() {
  background(255);
  fill(100);
  stroke(100);
  rect(width/2, height/2, 8.5, 3.5);
}

void mousePressed() {
  saveDepth(frame);
  frame++;
  //saveDepth(frameCount);//for animations
  
}

void saveDepth(int num) {
  loadPixels();
  float depth[] = new float[pixels.length];
  for (int i=0; i<pixels.length; i++)
    depth[i] = brightness(pixels[i])/255.0*5000.0;// map to 0mm-5000mm range, 0(BLACK)is 0m
  try {
    FileOutputStream fl = new FileOutputStream(dataPath("recording_"+num+".kinrec"));
    fl.write(0);//every kinect frame has a 0.0 at the beginning, not shure why but I replicated it
    fl.write(0);
    fl.write(0);
    fl.write(0);
    for (int i=0; i<depth.length; i++) {
      byte temp[] = ByteBuffer.allocate(4).putFloat(depth[i]).array();//convert the float(1000.22) to an byte array ({68, 122, 14, 20})or expresedin hex ({44 7a 0E 14})
      byte inverese[] = new byte[4];
      inverese[0] = temp[3];
      inverese[1] = temp[2];
      inverese[2] = temp[1];
      inverese[3] = temp[0];
      fl.write(inverese);
    }
    fl.close();
  }
  catch (IOException e) {
    System.out.print("Exception, an error ocurred!!\n");
    e.printStackTrace();
    System.out.println(e);
  }
}
