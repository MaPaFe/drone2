#include "kinectrecorder.h"
#include "kinectarea.h"
#include "system.h"
#include <array>
#include <fstream>
#include <iostream>
#include <string>

Kinect::Kinect()
    : m_shutdown(false),
      m_listener(libfreenect2::Frame::Depth) {

  m_device = nullptr;
  m_pipeline = nullptr;
  libfreenect2::setGlobalLogger(libfreenect2::createConsoleLogger(libfreenect2::Logger::Warning));
}

Kinect::~Kinect() {}

bool Kinect::connect() {
  if (m_freenect2.enumerateDevices() == 0) {
    std::cout << "[Error] No devices available" << std::endl;
    return false;
  }

  m_pipeline = new libfreenect2::OpenCLKdePacketPipeline();
  m_device = m_freenect2.openDevice(m_freenect2.getDefaultDeviceSerialNumber(), m_pipeline);

  if (m_device == nullptr) {
    std::cout << "[Error] Opening device" << std::endl;
    return false;
  }

  m_device->setIrAndDepthFrameListener(&m_listener);

  if (!m_device->startStreams(false, true)) {
    std::cout << "[Error] Starting device" << std::endl;
    return false;
  }

  return true;
}

bool Kinect::main(System& system) {
  while (!m_shutdown) {
    if (!m_listener.waitForNewFrame(m_frames, 1000 * 10)) {
      std::cout << "[Error] Recieving frames" << std::endl;
      return false;
    }
    libfreenect2::Frame* depth = m_frames[libfreenect2::Frame::Depth];
    // memcpy() copys byte for byte
    memcpy(system.m_depth, depth->data, 512 * 424 * 4);
    // copy data to drawArea's temporal array (4->size of float)
    if (frameCount < 300) {
      std::ofstream outfile;
      outfile.open("recording_" + std::to_string(frameCount++) + ".kinrec", std::fstream::binary);
      outfile.write((const char*)depth->data, 512 * 424 * 4);
      outfile.close();
    }
    for (size_t i = 0; i < 100; i++) {
      std::cout << "data: " << std::to_string(depth->data[i]) << std::endl;
    }
    system.new_frame(); // tell drawArea to update
    m_listener.release(m_frames);
  }

  m_device->stop();
  m_device->close();

  return true;
}

Sim::Vector Kinect::getPoint(const u_char* depth, int x, int y) const {
  std::cout << "getPoint depth              " << ((float*)depth)[512 * y + x] << std::endl;
  return Kinect::pixel_to_real(Sim::Vector(x, y, ((float*)depth)[512 * y + x]));
}

void Kinect::stop() { m_shutdown = true; }

// From https://openkinect.github.io/libfreenect2/classlibfreenect2_1_1Registration.html#a41a4d8479b97e4c74587ca431a94b2a3
Sim::Vector Kinect::pixel_to_real(Sim::Vector p) {
  // TODO use our own IrCameraParams
  // libfreenect2::Freenect2Device::IrCameraParams member

  // libfreenect2::Freenect2Device::IrCameraParams params(m_device->getIrCameraParams());
  // std::cout << params.fx << " " << params.fy << " " << params.cx << " " << params.cy << std::endl;
  // const float cx = params.cx, cy = params.cy;
  // const float fx = 1/params.fx, fy = 1/params.fy;

  const float cx = 256, cy = 212;
  const float fx = 364.18, fy = 364.33;
  Sim::Vector out;
  if (!std::isnan(p.z) && p.z > 0.0001f) {
    out.x = (p.x - cx) * p.z / fx;
    out.y = (p.y - cy) * p.z / fy;
    out.z = p.z;
  }
  return out;
}

Sim::Vector Kinect::real_to_pixel(Sim::Vector p) {
  const float cx = 256, cy = 212;
  const float fx = 364.18, fy = 364.33;
  Sim::Vector out;
  if (!std::isnan(p.z) && p.z > 0.0001f) {
    out.x = (p.x * fx / p.z) + cx;
    out.y = (p.y * fy / p.z) + cy;
    out.z = p.z;
  }
  return out;
}
