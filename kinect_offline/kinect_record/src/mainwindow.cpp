#include "mainwindow.h"
#include "graphsarea.h"
#include "kinectarea.h"
#include "system.h"
#include <iostream>

MainWindow::MainWindow()
    : m_Box_main(Gtk::ORIENTATION_HORIZONTAL),
      m_Box_buttons(Gtk::ORIENTATION_VERTICAL),
      m_Button_connect("Connect Kinect"),
      m_Button_find("Find"),
      m_Box_scales(Gtk::ORIENTATION_HORIZONTAL),
      m_Scale_min(Gtk::ORIENTATION_VERTICAL),
      m_Scale_max(Gtk::ORIENTATION_VERTICAL),
      m_AspectFrame("Kinect Depth", Gtk::ALIGN_CENTER, Gtk::ALIGN_CENTER, 512.0 / 424.0),
      // m_System(&m_KinectArea),
      m_System(&m_KinectArea, &m_GraphsArea),
      m_KinectArea(&m_System),
      m_GraphsArea(20 * 10) {

  set_title("Drone");
  set_border_width(10);

  add(m_Box_main);
  m_Box_main.pack_end(m_Box_buttons, Gtk::PACK_SHRINK);

  m_Box_buttons.set_layout(Gtk::BUTTONBOX_EXPAND);

  m_Box_buttons.pack_start(m_Button_connect, Gtk::PACK_EXPAND_WIDGET);
  m_Button_connect.signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::on_Button_connect_clicked));

  m_Box_buttons.pack_end(m_Button_find, Gtk::PACK_EXPAND_WIDGET);
  m_Button_find.signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::on_Button_find_clicked));

  m_Box_buttons.pack_end(m_Box_scales, Gtk::PACK_EXPAND_WIDGET);
  m_Box_scales.set_layout(Gtk::BUTTONBOX_EXPAND);
  m_Box_scales.pack_end(m_Scale_min, Gtk::PACK_EXPAND_WIDGET);

  m_Scale_min.set_value_pos(Gtk::POS_RIGHT);
  m_Scale_min.set_range(0.0, 5000.0);
  m_Scale_min.set_inverted();
  m_Scale_min.set_value(0.0);
  m_Scale_min.signal_value_changed().connect(sigc::mem_fun(*this, &MainWindow::on_Scale_min_changed));

  m_Box_scales.pack_end(m_Scale_max, Gtk::PACK_EXPAND_WIDGET);

  m_Scale_max.set_value_pos(Gtk::POS_RIGHT);
  m_Scale_max.set_range(0.0, 5000.0);
  m_Scale_max.set_inverted();
  m_Scale_max.set_value(2345.6);
  m_Scale_max.signal_value_changed().connect(sigc::mem_fun(*this, &MainWindow::on_Scale_max_changed));

  m_Box_main.pack_start(m_GraphsArea, Gtk::PACK_EXPAND_WIDGET);
  m_GraphsArea.set_size_request(512, 424);

  m_AspectFrame.add(m_KinectArea);
  m_KinectArea.set_size_request(512, 424);
  m_AspectFrame.set_border_width(0);
  m_Box_main.pack_start(m_AspectFrame, Gtk::PACK_EXPAND_WIDGET);

  show_all_children();
}

MainWindow::~MainWindow() {}

void MainWindow::on_Button_connect_clicked() {
  if (m_System.connect()) {
    m_Button_connect.set_label("Connected");
    m_Button_connect.set_sensitive(false);
  }
}

void MainWindow::on_Button_find_clicked() {
  // m_System.find();
  if (m_System.toggle_find())
    m_Button_find.set_label("Stop Find");
  else
    m_Button_find.set_label("Find");
}

void MainWindow::on_Scale_min_changed() { m_System.m_threshold_min = m_Scale_min.get_value(); }

void MainWindow::on_Scale_max_changed() { m_System.m_threshold_max = m_Scale_max.get_value(); }

void MainWindow::on_hide() { m_System.stop(); }
