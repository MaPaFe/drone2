#include "graphsarea.h"
#include <Simulator.h>
#include <chrono>
#include <iostream>
#include <math.h>
#include <string>

GraphsArea::GraphsArea(int point_count, bool draw_joysticks_)
    : m_point_count(point_count),
      draw_joysticks(draw_joysticks_),
      m_min(),
      m_max(),
      m_plots(6, std::vector<float>(point_count, 0)) {

  m_min.fill(10000);  // Set the "at least" for "minimum value" (10000 for full auto)
  m_max.fill(-10000); // Set the "at least" for "maximum value" (-10000 for full auto)

  for (size_t i = m_max.size() / 2; i < m_max.size(); i++) {
    m_max[i] = 127;
    m_min[i] = -127;
  }
}

GraphsArea::~GraphsArea() {}

void GraphsArea::add_position(Sim::Vector v) {
  // Update current
  m_current_position_pos++;
  m_current_position_pos %= m_point_count;
  // add data

  for (size_t i = 0; i < m_plots.size() / 2; i++)
    m_plots[i][m_current_position_pos] = v[i];

  // Update min/max for scaling
  for (size_t i = 0; i < 3; i++) {
    if (m_max[i] < v[i])
      m_max[i] = v[i];
    else if (m_min[i] > v[i])
      m_min[i] = v[i];
  }
  // Redraw
  queue_draw();
}

void GraphsArea::add_pid(Sim::Vector v) {
  // Update current
  m_current_position_pid++;
  m_current_position_pid %= m_point_count;
  // add data
  for (size_t i = m_plots.size() / 2; i < m_plots.size(); i++)
    m_plots[i][m_current_position_pos] = v[i];

  // Redraw
  queue_draw();
}

void GraphsArea::add_pid(int px, int py, int pz) {
  // Update current
  m_current_position_pid++;
  m_current_position_pid %= m_point_count;
  // add data
  m_plots[3][m_current_position_pid] = px;
  m_plots[4][m_current_position_pid] = py;
  m_plots[5][m_current_position_pid] = pz;
  // Redraw
  queue_draw();
}

void GraphsArea::joysticks(bool draw) {
  draw_joysticks = draw;
}

bool GraphsArea::on_draw(const Cairo::RefPtr<Cairo::Context>& cr) {
  auto t1_a = std::chrono::high_resolution_clock::now();
  Gtk::Allocation allocation = get_allocation();
  int width = allocation.get_width();
  int height = allocation.get_height();
  // Scale factor to fill the avalible space in x
  float scale_factor_x = (float)width / (float)(m_point_count - 1);
  // White Background
  cr->set_source_rgb(1.0f, 1.0f, 1.0f);
  cr->rectangle(0, 0, width, height);
  cr->fill();

  // Draw joysticks
  joystick(cr, width, height, std::min(width, height) / 10);

  // Black center line
  cr->set_line_join(Cairo::LINE_JOIN_ROUND);
  cr->set_line_width(4.0);
  cr->set_source_rgb(0.0f, 0.0f, 0.0f);
  cr->move_to(0, (float)height / 2.0f);
  cr->line_to(width, (float)height / 2.0f);
  cr->stroke();
  // Iterate over the plots and draw them (it looks so strange because the data
  // is stored in a "rolling buffer", first half, then second half)
  for (size_t j = 0; j < m_plots.size(); j++) {
    if (j >= m_plots.size() / 2) {
      // If pid translate
      cr->translate(0, (double)height / 2.0f);
      // Write 0/255 limits (once)
      if (j == m_plots.size() / 2) {
        cr->set_source_rgb(0.0f, 0.0f, 0.0f);
        float font_size = height / 40.0f;
        cr->set_font_size(font_size);
        cr->move_to(0, 4 + font_size);
        cr->show_text("127");
        cr->move_to(1, (-4) + height / 2.0f);
        cr->show_text("-127");
      }
    } else {
      // else draw dashed cero line
      cr->set_line_width(1.0);
      cr->set_dash(std::vector<double>{16.0f, 8.0f}, 0);
      cr->set_source_rgba(color[j][0], color[j][1], color[j][2], color[j][3]);
      cr->move_to(0, Sim::Utils::map(0, m_min[j], m_max[j], ((float)height / 2.0f), 0));
      cr->line_to(width, Sim::Utils::map(0, m_min[j], m_max[j], ((float)height / 2.0f), 0));
      cr->stroke();
      // Write max/min values
      cr->set_source_rgb(color[j][0], color[j][1], color[j][2]);
      float font_size = height / 40.0f;
      cr->set_font_size(font_size);
      cr->move_to(0, (m_plots.size() / 2) * font_size - j * font_size);
      cr->show_text(std::to_string((int)m_max[j]) + "mmm");
      cr->move_to(0, (-5) + height / 2.0f - j * font_size);
      cr->show_text(std::to_string((int)m_min[j]) + " mm");
    }
    // Draw actual graph
    cr->set_line_width(2.0);
    cr->set_dash(std::vector<double>{}, 0);
    cr->set_source_rgba(color[j][0], color[j][1], color[j][2], color[j][3]);
    cr->move_to(0, Sim::Utils::map(m_plots[j][0], m_min[j], m_max[j], ((float)height / 2.0f), 0));

    int x = 0; // to keep the x positoion "global" thruout the "rolling buffer"
    for (int i = m_current_position_pos; i >= 0; i--)
      cr->line_to(x++ * scale_factor_x, Sim::Utils::map(m_plots[j][i], m_min[j], m_max[j], ((float)height / 2.0f), 0));
    for (size_t i = m_plots[j].size() - 1; i > m_current_position_pos; i--)
      cr->line_to(x++ * scale_factor_x, Sim::Utils::map(m_plots[j][i], m_min[j], m_max[j], ((float)height / 2.0f), 0));
    cr->stroke();

    if (j >= m_plots.size() / 2) // If pid translate back
      cr->translate(0, -(double)height / 2.0f);
  }
  auto t2_a = std::chrono::high_resolution_clock::now();
  auto duration_array = std::chrono::duration_cast<std::chrono::milliseconds>(t2_a - t1_a).count();
  // std::cout << ": " << duration_array << std::endl;
  return true;
}

void GraphsArea::joystick(const Cairo::RefPtr<Cairo::Context>& cr, int w, int h, int radius) {
  float half_range = 1 / sqrt(2) * (radius - radius / 2.5);
  int x_pid = Sim::Utils::map(m_plots[3][m_current_position_pid], -127, 127, half_range, -half_range); //real-time red
  int y_pid = Sim::Utils::map(m_plots[4][m_current_position_pid], -127, 127, half_range, -half_range); //real-time red
  int z_pid = Sim::Utils::map(m_plots[5][m_current_position_pid], -127, 127, half_range, -half_range); //real-time red
  circle(cr, w, h, radius, radius, radius);                                                            // Big x/z
  circle(cr, w, h, radius - x_pid, radius - z_pid, radius / 2.5, false);                               // Small x/z
  circle(cr, w, h, radius * 3 + 10, radius, radius);                                                   // Big Y
  circle(cr, w, h, radius * 3 + 10, radius - y_pid, radius / 2.5, false);                              // Small Y
}

void GraphsArea::circle(const Cairo::RefPtr<Cairo::Context>& cr, int w, int h, int x_minus_width, int y_minus_height, int r, bool big) {
  int lw = 8;
  cr->set_line_width(lw);
  if (big)
    cr->set_source_rgba(0.3, 0.4, 0.6, 0.7);
  else
    cr->set_source_rgba(0.2, 0.26, 0.4, 0.7);
  cr->translate(w - x_minus_width - lw, h - y_minus_height - lw);
  cr->arc(0, 0, r, 0, 2 * M_PI);
  cr->stroke_preserve();
  if (big)
    cr->set_source_rgb(0.3, 0.4, 0.6);
  else
    cr->set_source_rgb(0.35, 0.47, 0.69);
  cr->fill();
  cr->translate(-w + x_minus_width + lw, -h + y_minus_height + lw);
}
