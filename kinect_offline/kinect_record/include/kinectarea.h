#pragma once

#include "system.h"
#include <gtkmm/drawingarea.h>
#include <opencv2/core.hpp>
#include <vector>

class KinectArea : public Gtk::DrawingArea {
public:
  explicit KinectArea(System* s);
  virtual ~KinectArea();

  void add_rect(Sim::Transform r);
  void clear_rects();

private:
  bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr) override;
  u_char* m_image_buffer; // For drawing in cairo
  Cairo::RefPtr<Cairo::ImageSurface> m_surface;
  System* p_system;
  std::vector<Sim::Transform> m_rects;
};
