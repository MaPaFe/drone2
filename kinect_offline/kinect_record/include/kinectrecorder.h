#pragma once

#include <Simulator.h>
#include <iostream>
#include <libfreenect2/frame_listener_impl.h>
#include <libfreenect2/libfreenect2.hpp>
#include <libfreenect2/logger.h>
#include <libfreenect2/registration.h>
#include <thread>

// Forward declarations
class System;

class Kinect {
public:
  Kinect();
  virtual ~Kinect();

  bool connect();
  bool main(System& system);
  Sim::Vector getPoint(const u_char* depth, int x, int y) const;
  void stop();
  libfreenect2::Freenect2Device* m_device;

  static Sim::Vector pixel_to_real(Sim::Vector p);
  static Sim::Vector real_to_pixel(Sim::Vector p);

private:
  bool m_shutdown;
  libfreenect2::Freenect2 m_freenect2;
  libfreenect2::PacketPipeline* m_pipeline;
  libfreenect2::SyncMultiFrameListener m_listener;
  libfreenect2::FrameMap m_frames;
  int frameCount{0};
};
