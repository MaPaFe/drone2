#pragma once

#include "graphsarea.h"
#include "kinectarea.h"
#include "kinectrecorder.h"
#include "system.h"
#include <gtkmm.h>

class MainWindow : public Gtk::Window {
public:
  MainWindow();
  virtual ~MainWindow();

private:
  void on_Button_connect_clicked();
  void on_Button_find_clicked();
  void on_Scale_min_changed();
  void on_Scale_max_changed();
  void on_hide() override;

  // Member data
  Gtk::Box m_Box_main;
  Gtk::AspectFrame m_AspectFrame;
  Gtk::ButtonBox m_Box_buttons;
  Gtk::Button m_Button_connect;
  Gtk::Button m_Button_find;
  Gtk::ButtonBox m_Box_scales;
  Gtk::Scale m_Scale_min;
  Gtk::Scale m_Scale_max;

  System m_System;
  KinectArea m_KinectArea;
  GraphsArea m_GraphsArea;
};
