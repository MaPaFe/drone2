#pragma once

#include "graphsarea.h"
#include "kinectrecorder.h"
#include <Simulator.h>
#include <opencv2/core.hpp>
#include <thread>

// Forward declaration
class KinectArea;

class System {
public:
  explicit System(KinectArea* d, GraphsArea* p);
  // explicit System(KinectArea* d);
  virtual ~System();

  bool connect();
  void stop();

  void main_loop();
  void find();
  void new_frame();

  bool toggle_find();

  u_char* m_depth; // Raw depth data from Kinect

  float m_threshold_min;
  float m_threshold_max;

private:
  Sim::Transform box_search(cv::Mat& depth_mat, Sim::Transform prediction) const;

  // For drawing
  KinectArea* p_drawer;
  GraphsArea* p_plot;

  // For managing Kinect
  Kinect m_Kinect;
  std::thread* m_Kinect_Thread;

  std::vector<Sim::Drone> m_drones;
  bool find_on;
};
