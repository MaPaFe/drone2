#pragma once

#include "system.h"
#include <gtkmm/drawingarea.h>
#include <opencv2/core.hpp>
#include <vector>

class KinectArea : public Gtk::DrawingArea {
public:
  explicit KinectArea(System* s);
  virtual ~KinectArea();

  void set_drone(Sim::Transform rect_drone);
  void set_setpoint(Sim::Transform rect_setpoint);
  
  Sim::Transform m_rect_drone, m_rect_setpoint;

private:
  bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr) override;
  u_char* m_image_buffer; // For drawing in cairo
  Cairo::RefPtr<Cairo::ImageSurface> m_surface;
  System* p_system;
  std::mutex m_mutex_data;
};
