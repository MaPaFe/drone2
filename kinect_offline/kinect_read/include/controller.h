#pragma once

#include <iostream>
#include <gainput/gainput.h>
#include <Simulator.h>

class Controller {
public:
  Controller();
  virtual ~Controller();

  void update();

  Sim::Vector setpoint;
  float throttle, pitch, roll, yaw;
  bool isManual;

private:
  enum Buttons { Throttle, Pitch, Roll, Yaw, X };
  gainput::InputManager manager;
  gainput::InputMap map;
  gainput::DeviceId id;

  time_point last_press;
};
