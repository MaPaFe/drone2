#pragma once

#include <Simulator.h>
#include <fstream>
#include <iostream>
#include <libfreenect2/frame_listener_impl.h>
#include <libfreenect2/libfreenect2.hpp>
#include <libfreenect2/logger.h>
#include <libfreenect2/registration.h>
#include <thread>

// Forward declarations
class System;

class Kinect { //OFFLINE
public:
  Kinect();
  virtual ~Kinect();

  bool connect();
  bool main(System& system);
  Sim::Vector getPoint(const u_char* depth, int x, int y) const;
  void stop();
  libfreenect2::Freenect2Device* m_device;

  static Sim::Vector pixel_to_real(Sim::Vector p);
  static Sim::Transform pixel_to_real(Sim::Transform t);
  static Sim::Vector real_to_pixel(Sim::Vector p);
  static Sim::Transform real_to_pixel(Sim::Transform t);

private:
  bool m_shutdown;
  int frameCount{0};
  std::ifstream infile;
  char data[512 * 424 * 4];
};
