#pragma once

#include <Simulator.h>
#include <array>
#include <gtkmm-3.0/gtkmm/drawingarea.h>
#include <gtkmm.h>
#include <vector>

// TODO if NaN white space
// Draws plots with Cairo. pos and pid have to be in symetric amounts,
// all pos first, then coresponding pid

class View3DArea : public Gtk::DrawingArea {
public:
  View3DArea();
  virtual ~View3DArea();

  void add_position(Sim::Vector v);
  void set_focal(float f);
  void set_x_rotation(float r);
  void set_y_rotation(float r);
  void set_z_rotation(float r);

private:
  bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr) override;
  Cairo::RefPtr<Cairo::Context> const* m_cr;
  int width = 0;
  int height = 0;
  std::array<float, 3> translation{100, 100, 100}; //x ,y, z
  std::array<float, 3> rotation{0, 0, 0};          //x,y,z
  float focal = 10.0;
  std::array<float, 2> tod(std::array<float, 3>);
  std::array<float, 2>
  tod(float x, float y, float z);
  void axes();
  void line(float x1, float y1, float z1, float x2, float y2, float z2);
  std::array<std::array<float, 4>, 6>
      color{{                // r, g, b, a
             1, 0, 0, 0.7,   // red
             0, .8, 0, 0.7,  // green
             0, 0, 1, 0.7,   // blue
             1, 0, 0, 0.5,   // light red
             0, .8, 0, 0.5,  // light green
             0, 0, 1, 0.5}}; // light blue
};
