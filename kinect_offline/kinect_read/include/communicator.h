#pragma once

#include "cArduino.h"
#include <Simulator.h>
#include <iostream>

class Communicator {
public:
  Communicator();
  virtual ~Communicator();
  void send_payload(int channel, double throttle, double pitch, double roll, double yaw);
  void send_payload(int channel, Sim::Vector v);
  void conect();
  void disconect();
  bool isOpen();

private:
  cArduino arduino; // has a serial receive buffer, which holds 64 bytes.
  static const ArduinoBaundRate baud = ArduinoBaundRate::B4000000bps;
  bool arduinoOpen = false;
};
