#pragma once

#include <Simulator.h>
#include <array>
#include <gtkmm-3.0/gtkmm/drawingarea.h>
#include <gtkmm.h>
#include <vector>

// TODO if NaN white space
// Draws plots with Cairo. pos and pid have to be in symetric amounts,
// all pos first, then coresponding pid
class GraphsArea : public Gtk::DrawingArea {
public:
  GraphsArea(bool draw_joysticks_ = true);
  virtual ~GraphsArea();

  void add_position(Sim::Vector v);
  void add_pid(Sim::Vector v);
  void add_pid(int px, int py, int pz);
  void joysticks(bool draw); // Visulise with virtual joystick ;)

private:
  bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr) override;
  void joystick(const Cairo::RefPtr<Cairo::Context>& cr, int w, int h, int radius);
  void circle(const Cairo::RefPtr<Cairo::Context>& cr, int w, int h, int x_minus_width, int y_minus_height, int r, bool big = true); // minus_width is how many pixels away from border
  std::array<float, 6> m_min;
  std::array<float, 6> m_max;
  std::array<std::array<float, 10 * 30>, 6> m_plots;
  bool draw_joysticks;
  std::array<std::array<float, 4>, 6> color{{                // r, g, b, a,
                                             1, 0, 0, 0.7,   // red
                                             0, .8, 0, 0.7,  // green
                                             0, 0, 1, 0.7,   // blue
                                             1, 0, 0, 0.5,   //
                                             0, .8, 0, 0.5,  //
                                             0, 0, 1, 0.5}}; //
  int m_current_position_pos = -1;
  int m_current_position_pid = -1;
  std::mutex m_mutex_data;
};
