#include "mainwindow.h"
#include "graphsarea.h"
#include "kinectarea.h"
#include "system.h"
#include <iostream>

MainWindow::MainWindow()
    : m_Box_main(Gtk::ORIENTATION_HORIZONTAL),
      m_Box_buttons(Gtk::ORIENTATION_VERTICAL),
      m_Button_connect("Connect Kinect"),
      m_Button_find("Find"),
      m_Box_scales(Gtk::ORIENTATION_HORIZONTAL),
      m_Scale_min(Gtk::ORIENTATION_VERTICAL),
      m_Scale_max(Gtk::ORIENTATION_VERTICAL),
      m_AspectFrame("Kinect Depth", Gtk::ALIGN_CENTER, Gtk::ALIGN_CENTER, 512.0 / 424.0),
      // m_System(&m_KinectArea),//
      m_System(&m_KinectArea, &m_GraphsArea),
      m_KinectArea(&m_System),
      m_GraphsArea(),
      //m_View3DArea(),
      m_Scale_View3DArea_focal(Gtk::ORIENTATION_HORIZONTAL),
      m_Scale_View3DArea_x_rotation(Gtk::ORIENTATION_HORIZONTAL),
      m_Scale_View3DArea_y_rotation(Gtk::ORIENTATION_HORIZONTAL),
      m_Scale_View3DArea_z_rotation(Gtk::ORIENTATION_HORIZONTAL),
      m_Button_load_pids("load pids"),
      m_Button_update_pids("update pids"),
      m_Button_save_pids("save pids, click update first") {

  m_System_Thread = nullptr;

  set_title("Drone");
  // set_icon_from_file("icon_Zitronchen.png");
  set_border_width(10);

  add(m_Box_main);
  // PIDs
  m_Box_main.pack_end(m_pids_outside_Box, Gtk::PACK_SHRINK, 5);
  m_pids_outside_Box.set_orientation(Gtk::ORIENTATION_VERTICAL);
  // PIDs managment buttons
  m_pids_outside_Box.pack_start(m_BBox_manage_pids, Gtk::PACK_SHRINK);
  m_BBox_manage_pids.set_orientation(Gtk::ORIENTATION_HORIZONTAL);
  // PIDs load
  m_BBox_manage_pids.pack_start(m_Button_load_pids);
  m_Button_load_pids.signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::on_Button_load_pids_clicked));
  // PIDs update
  m_BBox_manage_pids.pack_start(m_Button_update_pids);
  m_Button_update_pids.signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::on_Button_update_pids_clicked));
  // PIDs save
  m_BBox_manage_pids.pack_start(m_Button_save_pids);
  m_Button_save_pids.signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::on_Button_save_pids_clicked));
  // PIDs coefficient grid
  m_pids_outside_Box.pack_start(m_pids_outside_Grid, Gtk::PACK_EXPAND_WIDGET);
  m_pids_outside_Grid.set_row_homogeneous(true);
  m_pids_outside_Grid.set_column_homogeneous(true);
  // Populate grid with a vertical box with an label and entry
  int a = -1;
  int b = 0;
  for (size_t i = 0; i < m_pids_Entry.size(); i++) {
    if (a > 1) {
      a = 0;
      b > 1 ? b = 0 : b++;
    } else {
      a++;
    }
    m_pids_outside_Grid.attach(m_pids_Box[i], a + 1, b + 1, 1, 1);
    m_pids_Box[i].set_orientation(Gtk::ORIENTATION_VERTICAL);
    m_pids_Box[i].pack_start(m_pids_Labels[i], Gtk::PACK_SHRINK);
    m_pids_Labels[i].set_label(m_pid_labels_data[i]);
    m_pids_Box[i].pack_start(m_pids_Entry[i], Gtk::PACK_EXPAND_WIDGET);
    m_pids_Entry[i].set_width_chars(3);
    m_pids_Entry[i].set_text("1,1"); // Waiting to load pids
  }
  // // View3DArea
  // m_pids_outside_Box.pack_end(m_View3DArea, Gtk::PACK_EXPAND_WIDGET);
  // // Scale_View3DArea_focal
  // m_pids_outside_Box.pack_end(m_Scale_View3DArea_focal, Gtk::PACK_SHRINK);
  // m_Scale_View3DArea_focal.set_value_pos(Gtk::POS_RIGHT);
  // m_Scale_View3DArea_focal.set_range(0.0, 200);
  // m_Scale_View3DArea_focal.set_value(10.0);
  // m_Scale_View3DArea_focal.signal_value_changed().connect(sigc::mem_fun(*this, &MainWindow::on_Scale_View3DArea_focal_changed));
  // // m_Scale_View3DArea_z_rotation
  // m_pids_outside_Box.pack_end(m_Scale_View3DArea_z_rotation, Gtk::PACK_SHRINK);
  // m_Scale_View3DArea_z_rotation.set_value_pos(Gtk::POS_RIGHT);
  // m_Scale_View3DArea_z_rotation.set_range(-G_PI, G_PI);
  // m_Scale_View3DArea_z_rotation.set_value(0.0);
  // m_Scale_View3DArea_z_rotation.signal_value_changed().connect(sigc::mem_fun(*this, &MainWindow::on_Scale_View3DArea_z_rotation_changed));
  // m_Scale_View3DArea_z_rotation.add_mark(0.0, Gtk::POS_TOP, "z");
  // // m_Scale_View3DArea_y_rotation
  // m_pids_outside_Box.pack_end(m_Scale_View3DArea_y_rotation, Gtk::PACK_SHRINK);
  // m_Scale_View3DArea_y_rotation.set_value_pos(Gtk::POS_RIGHT);
  // m_Scale_View3DArea_y_rotation.set_range(-G_PI, G_PI);
  // m_Scale_View3DArea_y_rotation.set_value(0.0);
  // m_Scale_View3DArea_y_rotation.signal_value_changed().connect(sigc::mem_fun(*this, &MainWindow::on_Scale_View3DArea_y_rotation_changed));
  // m_Scale_View3DArea_y_rotation.add_mark(0.0, Gtk::POS_TOP, "y");
  // // m_Scale_View3DArea_x_rotation
  // m_pids_outside_Box.pack_end(m_Scale_View3DArea_x_rotation, Gtk::PACK_SHRINK);
  // m_Scale_View3DArea_x_rotation.set_value_pos(Gtk::POS_RIGHT);
  // m_Scale_View3DArea_x_rotation.set_range(-G_PI, G_PI);
  // m_Scale_View3DArea_x_rotation.set_value(0.0);
  // m_Scale_View3DArea_x_rotation.signal_value_changed().connect(sigc::mem_fun(*this, &MainWindow::on_Scale_View3DArea_x_rotation_changed));
  // m_Scale_View3DArea_x_rotation.add_mark(0.0, Gtk::POS_TOP, "x");

  // Kinect managment buttons/slider
  m_Box_main.pack_end(m_Box_buttons, Gtk::PACK_SHRINK);

  m_Box_buttons.set_layout(Gtk::BUTTONBOX_EXPAND);

  m_Box_buttons.pack_start(m_Button_connect, Gtk::PACK_EXPAND_WIDGET);
  m_Button_connect.signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::on_Button_connect_clicked));

  m_Box_buttons.pack_end(m_Button_find, Gtk::PACK_EXPAND_WIDGET);
  m_Button_find.signal_clicked().connect(sigc::mem_fun(*this, &MainWindow::on_Button_find_clicked));

  m_Box_buttons.pack_end(m_Box_scales, Gtk::PACK_EXPAND_WIDGET);
  m_Box_scales.set_layout(Gtk::BUTTONBOX_EXPAND);
  m_Box_scales.pack_end(m_Scale_min, Gtk::PACK_EXPAND_WIDGET);

  m_Scale_min.set_value_pos(Gtk::POS_RIGHT);
  m_Scale_min.set_range(0.0, 5000.0);
  m_Scale_min.set_inverted();
  m_Scale_min.set_value(m_System.m_threshold_min);
  m_Scale_min.signal_value_changed().connect(sigc::mem_fun(*this, &MainWindow::on_Scale_min_changed));

  m_Box_scales.pack_end(m_Scale_max, Gtk::PACK_EXPAND_WIDGET);

  m_Scale_max.set_value_pos(Gtk::POS_RIGHT);
  m_Scale_max.set_range(0.0, 5000.0);
  m_Scale_max.set_inverted();
  m_Scale_max.set_value(m_System.m_threshold_max);
  m_Scale_max.signal_value_changed().connect(sigc::mem_fun(*this, &MainWindow::on_Scale_max_changed));

  m_Box_main.pack_start(m_GraphsArea, Gtk::PACK_EXPAND_WIDGET);
  m_GraphsArea.set_size_request(512, 424);

  m_AspectFrame.add(m_KinectArea);
  m_KinectArea.set_size_request(512, 424);
  m_AspectFrame.set_border_width(0);
  m_Box_main.pack_start(m_AspectFrame, Gtk::PACK_EXPAND_WIDGET);

  show_all_children();
}

MainWindow::~MainWindow() {}

void MainWindow::on_Button_connect_clicked() {
  if (m_System.connect()) {
    m_Button_connect.set_label("Connected");
    m_Button_connect.set_sensitive(false);
    if (m_System_Thread == nullptr) {
      m_System_Thread = new std::thread([this] { m_System.main_loop(); });
    }
  }
}

void MainWindow::on_Button_find_clicked() {
  // m_System.find();
  if (m_System.toggle_find()) {
    m_Button_find.set_label("Stop Find");
  } else {
    m_Button_find.set_label("Find");
  }
}

void MainWindow::on_Button_load_pids_clicked() {
  m_System.load_pids(0);
  for (size_t i = 0; i < m_pids_Entry.size(); i++) {
    m_pids_Entry[i].set_text(std::to_string(m_System.pids[0][i]));
  }
}

void MainWindow::on_Button_update_pids_clicked() {
  std::array<double, 9> buf;
  for (size_t i = 0; i < m_pids_Entry.size(); i++) {
    buf[i] = std::atof(m_pids_Entry[i].get_text().c_str());
  }
  m_System.update_pids(0, buf);
}

void MainWindow::on_Button_save_pids_clicked() {
  m_System.save_pids(0);
}

void MainWindow::on_Scale_min_changed() { m_System.m_threshold_min = m_Scale_min.get_value(); }

void MainWindow::on_Scale_max_changed() { m_System.m_threshold_max = m_Scale_max.get_value(); }

void MainWindow::on_Scale_View3DArea_focal_changed() {
  m_View3DArea.set_focal(m_Scale_View3DArea_focal.get_value());
  m_View3DArea.queue_draw();
}
void MainWindow::on_Scale_View3DArea_x_rotation_changed() {
  m_View3DArea.set_x_rotation(m_Scale_View3DArea_x_rotation.get_value());
  m_View3DArea.queue_draw();
}
void MainWindow::on_Scale_View3DArea_y_rotation_changed() {
  m_View3DArea.set_y_rotation(m_Scale_View3DArea_y_rotation.get_value());
  m_View3DArea.queue_draw();
}
void MainWindow::on_Scale_View3DArea_z_rotation_changed() {
  m_View3DArea.set_z_rotation(m_Scale_View3DArea_z_rotation.get_value());
  m_View3DArea.queue_draw();
}

void MainWindow::on_hide() { m_System.stop(); }
