#include "kinect.h"
#include "kinectarea.h"
#include "system.h"
#include <array>
#include <fstream>
#include <iostream>
#include <string>
//OFFLINE
Kinect::Kinect() {}

Kinect::~Kinect() {}

bool Kinect::connect() {
  return true;
}

bool Kinect::main(System& system) {
  while (!m_shutdown) {
    // std::cout << "recording_" + std::to_string(frameCount) + ".kinrec" << std::endl;
    infile.open("recording_" + std::to_string(frameCount++) + ".kinrec", std::fstream::binary);
    if (infile.fail()) {
      // std::cerr << "Error: " << std::strerror(errno) << std::endl;
      char* name = get_current_dir_name();
      // std::cout << "Current Working Dir: " << name << "\n";
      free(name);
      frameCount = 0;
    }
    infile.read(data, 512 * 424 * 4);
    infile.close();
    // std::cout << "recording_" + std::to_string(frameCount) + ".kinrec" << std::endl;
    // infile.open("recording_" + std::to_string(frameCount++) + ".kinrec", std::fstream::binary);
    // //infile.open("recording_0.kinrec", std::fstream::binary);
    // infile.read(data, 512 * 424 * 4);
    // if (infile.good())
    //   std::cout << "infile.good" << (bool)infile.good() << std::endl;
    // infile.close();

    // memcpy() copys byte for byte
    memcpy(system.m_depth, &data, 512 * 424 * 4); //fom data to system.m_depth
    system.new_frame();                           // tell drawArea to update
    // copy data to drawArea's temporal array (4->size of float)
    std::this_thread::sleep_for(std::chrono::milliseconds(20));
    //wait or do something
  }
  return true;
}

Sim::Vector Kinect::getPoint(const u_char* depth, int x, int y) const {
  std::cout << "getPoint depth              " << ((float*)depth)[512 * y + x] << std::endl;
  return Kinect::pixel_to_real(Sim::Vector(x, y, ((float*)depth)[512 * y + x]));
}

void Kinect::stop() { m_shutdown = true; }

// From https://openkinect.github.io/libfreenect2/classlibfreenect2_1_1Registration.html#a41a4d8479b97e4c74587ca431a94b2a3
Sim::Vector Kinect::pixel_to_real(Sim::Vector p) {
  // TODO use our own IrCameraParams
  // libfreenect2::Freenect2Device::IrCameraParams member

  // libfreenect2::Freenect2Device::IrCameraParams params(m_device->getIrCameraParams());
  // std::cout << params.fx << " " << params.fy << " " << params.cx << " " << params.cy << std::endl;
  // const float cx = params.cx, cy = params.cy;
  // const float fx = 1/params.fx, fy = 1/params.fy;

  const float cx = 256, cy = 212;
  const float fx = 364.18, fy = 364.33;
  Sim::Vector out;
  if (!std::isnan(p.z) && p.z > 0.0001f) {
    out.x = (p.x - cx) * p.z / fx;
    out.y = (p.y - cy) * p.z / fy;
    out.z = p.z;
  }
  return out;
}

Sim::Transform Kinect::pixel_to_real(Sim::Transform t) {
  // To measure: at distance ex. 1500, the drone is measured at 24 x-wide and 8 y-height at that distance. It follows that distance(1500) * width|height(24|8) / p.z the pespective corrects.
  // The focal lenght is canceled out (projection=(focal/p.z)*measured_width*(measured_depth/focal))
  const float fx = 364.18, fy = 364.33;
  Sim::Transform out;
  out.position = pixel_to_real(t.position);
  if (!std::isnan(out.position.z) && out.position.z > 0.0001f) {
    out.size.x = t.size.x * t.position.z / fx;
    out.size.y = t.size.y * t.position.z / fy;
  }
  return out;
}

Sim::Vector Kinect::real_to_pixel(Sim::Vector p) {
  const float cx = 256, cy = 212;
  const float fx = 364.18, fy = 364.33;
  Sim::Vector out;
  if (!std::isnan(p.z) && p.z > 0.0001f) {
    out.x = (p.x * fx / p.z) + cx;
    out.y = (p.y * fy / p.z) + cy;
    out.z = p.z;
  }
  return out;
}

Sim::Transform Kinect::real_to_pixel(Sim::Transform t) {
  const float fx = 364.18, fy = 364.33;
  Sim::Transform out;
  out.position = real_to_pixel(t.position);
  if (!std::isnan(out.position.z) && out.position.z > 0.0001f) {
    out.size.x = t.size.x * fx / t.position.z;
    out.size.y = t.size.y * fy / t.position.z;
  }
  return out;
}
