#include "view3DArea.h"
#include <Simulator.h>
#include <array>
#include <chrono>
#include <cmath>
#include <iostream>
#include <string>

View3DArea::View3DArea() : m_cr(nullptr) {
}

View3DArea::~View3DArea() {}
void View3DArea::set_focal(float f) {
  focal = f;
}
void View3DArea::set_x_rotation(float r) {
  rotation[0] = r;
}
void View3DArea::set_y_rotation(float r) {
  rotation[1] = r;
}
void View3DArea::set_z_rotation(float r) {
  rotation[2] = r;
}
bool View3DArea::on_draw(const Cairo::RefPtr<Cairo::Context>& cr) {
  m_cr = &cr; // get context so it can be used by the rest of drawing functions
  auto t1_a = std::chrono::high_resolution_clock::now();
  Gtk::Allocation allocation = get_allocation();
  width = allocation.get_width();
  height = allocation.get_height();
  // White Background
  cr->set_source_rgb(1.0f, 1.0f, 1.0f);
  cr->rectangle(0, 0, width, height);
  cr->fill();
  // put the 0,0 in the center
  cr->translate(width / 2, height / 2);

  // rotation[0] = rotation[0] > G_PI ? 0 : rotation[0] + 0.1;
  // rotation[1] = rotation[1] > G_PI ? 0 : rotation[1] + 0.1;
  // rotation[2] += .01;

  axes();
  //line(100, 0, 0, 70, 70, 70);

  auto t2_a = std::chrono::high_resolution_clock::now();
  auto duration_array = std::chrono::duration_cast<std::chrono::milliseconds>(t2_a - t1_a).count();
  // std::cout << ": " << duration_array << std::endl;
  return true;
}
std::array<float, 2> View3DArea::tod(std::array<float, 3> pos) { // convert 3d to 2d with translation, rotation and projection
  // Translation
  pos[0] += translation[0];
  pos[1] += translation[1];
  pos[2] += translation[2];
  // Rotation
  //https://en.wikipedia.org/wiki/3D_projection#Perspective_projection
  pos[0] = std::cos(rotation[1]) * (std::sin(rotation[2]) * pos[1] + std::cos(rotation[2]) * pos[0]) - std::sin(rotation[1]) * pos[2];
  pos[1] = std::sin(rotation[0]) * (std::cos(rotation[1]) * pos[2] + std::sin(rotation[1]) * (std::sin(rotation[2]) * pos[1] + std::cos(rotation[2]) * pos[0])) + std::cos(rotation[0]) * (std::cos(rotation[2]) * pos[1] - std::sin(rotation[2]) * pos[0]);
  pos[2] = std::cos(rotation[0]) * (std::cos(rotation[1]) * pos[2] + std::sin(rotation[1]) * (std::sin(rotation[2]) * pos[1] + std::cos(rotation[2]) * pos[0])) - std::sin(rotation[0]) * (std::cos(rotation[2]) * pos[1] - std::sin(rotation[2]) * pos[0]);
  //backup:
  //  x_ = cyr*(szr*y_+czr*x_)-syr*z_;
  // y_ = sxr*(cyr*z_+syr*(szr*y_+czr*x_))+cxr*(czr*y_-szr*x_);
  // z_ = cxr*(cyr*z_+syr*(szr*y_+czr*x_))-sxr*(czr*y_-szr*x_);

  // X-rotation
  // pos[0] = pos[0];
  // pos[1] = pos[1] * std::cos(rotation[0]) - std::sin(rotation[0]) * pos[2];
  // pos[2] = pos[1] * std::sin(rotation[0]) + std::cos(rotation[0]) * pos[2];
  // Y-rotation
  // pos[0] = pos[0] * std::cos(rotation[1]) + std::sin(rotation[1]) * pos[2];
  // pos[1] = pos[1];
  // pos[2] = pos[0] * -std::sin(rotation[1]) + std::cos(rotation[1]) * pos[2];
  // Z-rotation
  // pos[0] = pos[0] * std::cos(rotation[2]) - std::sin(rotation[2]) * pos[1];
  // pos[1] = pos[0] * std::sin(rotation[2]) + std::cos(rotation[2]) * pos[1];
  // pos[2] = pos[2];

  // Projection
  std::array<float, 2> out{pos[0], pos[1]};
  if (focal != 0.0 && pos[2] != 0) { // z can't be 0
    if (pos[0] != 0)
      out[0] = (focal / pos[2]) * pos[0];
    if (pos[1] != 0)
      out[1] = (focal / pos[2]) * pos[1];
  }
  return out;
}
std::array<float, 2> View3DArea::tod(float x, float y, float z) { // convert 3d to 2d with translation, rotation and projection
  return tod(std::array<float, 3>{
      x, y, z});
}
void View3DArea::axes() {
  (*m_cr)->set_source_rgb(1.0f, 0.0f, 0.0f);
  line(0, 0, 0, 100, 0, 0); //
  (*m_cr)->set_source_rgb(0.0f, 1.0f, 0.0f);
  line(0, 0, 0, 0, 100, 0); //
  (*m_cr)->set_source_rgb(0.0f, 0.0f, 1.0f);
  line(0, 0, 0, 0, 0, 100); //

  (*m_cr)->set_source_rgb(0.0f, 0.0f, 0.0f);
  line(100, 0, 0, 100, 0, 100);
  line(100, 0, 100, 0, 0, 100);
  line(0, 0, 100, 0, 100, 100);
  line(0, 100, 100, 0, 100, 0);
  line(0, 100, 0, 100, 100, 0);
  line(100, 100, 0, 100, 0, 0);
}
void View3DArea::line(float x1, float y1, float z1, float x2, float y2, float z2) {
  std::array<float, 2> p1 = tod(x1, y1, z1);
  std::array<float, 2> p2 = tod(x2, y2, z2);
  (*m_cr)->set_line_width(1.0);
  (*m_cr)->set_line_join(Cairo::LINE_JOIN_ROUND);
  (*m_cr)->move_to(p1[0], p1[1]);
  (*m_cr)->line_to(p2[0], p2[1]);
  (*m_cr)->stroke();
}
