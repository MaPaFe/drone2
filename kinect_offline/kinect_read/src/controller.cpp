#include "controller.h"
#include <gainput/gainput.h>

Controller::Controller()
    : map(manager),
      setpoint(0, 0, 1960),
      isManual(false) {
  id = manager.CreateDevice<gainput::InputDevicePad>();

  map.MapFloat(Throttle, id, gainput::PadButtonLeftStickY, 0, -127.5); // throttle
  map.MapFloat(Pitch, id, gainput::PadButtonAxis4, 0, 127.5);          // pitch
  map.MapFloat(Roll, id, gainput::PadButtonRightStickY, 0, 127.5);     // roll
  map.MapFloat(Yaw, id, gainput::PadButtonLeftStickX, 0, -127.5);      // yaw
  map.MapBool(X, id, gainput::PadButtonSelect);                        // X button
}

Controller::~Controller() {}

void Controller::update() {
  manager.Update();

  throttle = map.GetFloat(Throttle);
  pitch = map.GetFloat(Pitch);
  roll = map.GetFloat(Roll);
  yaw = map.GetFloat(Yaw);

  if (map.GetBool(X) && Sim::Utils::diff_time(Sim::Utils::time_now(), last_press) > std::chrono::milliseconds(500)) {
    isManual = !isManual;
    last_press = Sim::Utils::time_now();
  }

  if (!isManual) {
    setpoint.x += roll / 100;
    setpoint.y += pitch / 100;
    setpoint.z -= throttle / 100;
  }
}
