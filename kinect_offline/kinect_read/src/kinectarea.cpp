#include "kinectarea.h"
#include <cairomm/context.h> //on_draw

KinectArea::KinectArea(System* s)
  : p_system(s) {

  m_image_buffer = new unsigned char[512 * 424 * 4];
  // Is equal to 4bytes*512
  int stride = Cairo::ImageSurface::format_stride_for_width(Cairo::FORMAT_ARGB32, 512);
  m_surface = Cairo::ImageSurface::create(m_image_buffer, Cairo::FORMAT_ARGB32, 512, 424, stride);
}

KinectArea::~KinectArea() {
  delete[] m_image_buffer;
}

void KinectArea::set_drone(Sim::Transform rect_drone){
    std::lock_guard<std::mutex> guard(m_mutex_data);
    m_rect_drone = rect_drone;
}

void KinectArea::set_setpoint(Sim::Transform  rect_setpoint){
    std::lock_guard<std::mutex> guard(m_mutex_data);
    m_rect_setpoint =  rect_setpoint;
}

bool KinectArea::on_draw(const Cairo::RefPtr<Cairo::Context>& cr) {
  std::lock_guard<std::mutex> guard(m_mutex_data);
  Gtk::Allocation allocation = get_allocation();
  const int width = allocation.get_width();
  const int height = allocation.get_height();

  cr->scale(width / 512.0, height / 424.0);

  // copy data from temp[] to data[] while transforming "types"
  // temp: 4bytes -> float -> distanceZ in mm
  // data:
  // 4bytes -> colorARGB-> 8bits(Alpha,unused) + 8bits(Red) + 8bits(Green) +
  // 8bits(Blue) whe want to see grayscale, so we set the red, green and blue to
  // same values
  /// float max = 0;
  for (size_t i = 0; i < 512 * 424; i++) {
    // first we cast the unsigned char to a float:f = ((float*)temp)[i];
    // then we map the value to an 8 bit int (0-255 brightness): brightness = f/8000,
    // would give (0-1) -> brightness = 255*(f/8000) give finaly (0-255)
    /// if (max < ((float *)m_image_buffer)[i])max = ((float *)m_image_buffer)[i];

    const float depth = ((float*)p_system->m_depth)[i];
    u_int8_t brightness;
    if (depth < p_system->m_threshold_max && depth > p_system->m_threshold_min)
      brightness = 255 * depth / 5000.0;
    else
      brightness = 0;

    // brightness = 255 * (((float *)m_image_buffer)[i] - p_system->m_threshold_min) / (p_system->m_threshold_max - p_system->m_threshold_min);

    m_image_buffer[i * 4 + 3] = 255;        // Alpha=ff
    m_image_buffer[i * 4 + 2] = brightness; // Red
    m_image_buffer[i * 4 + 1] = brightness; // Green
    m_image_buffer[i * 4 + 0] = brightness; // Blue
  }
  m_surface->mark_dirty(); // tell cairo we have modified data[]
  cr->set_source(m_surface, 0, 0);
  cr->rectangle(0, 0, width, height);
  cr->fill();

  cr->set_line_width(2);
  cr->set_line_join(Cairo::LINE_JOIN_ROUND);
  // Drone rect
  Sim::Transform t = Kinect::real_to_pixel(m_rect_drone);
  cr->rectangle(t.position.x - t.size.x / 2, t.position.y - t.size.y / 2, t.size.x, t.size.y);
  // std::cout << t << std::endl;
  cr->set_source_rgba(0, 1, 0, 1);
  cr->stroke_preserve();
  cr->set_source_rgba(0, 1, 0, 0.25);
  cr->fill();

  // Setpoint rect
  t = Kinect::real_to_pixel(m_rect_setpoint);
  cr->rectangle(t.position.x - t.size.x / 2, t.position.y - t.size.y / 2, t.size.x, t.size.y);
  // // std::cout << r << std::endl;
  cr->set_source_rgba(1, 0, 0, 1);
  cr->stroke_preserve();
  cr->set_source_rgba(1, 0, 0, 0.25);
  cr->fill();

  return true;
}
