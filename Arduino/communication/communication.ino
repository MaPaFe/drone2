#include <SPI.h>

#define ARR_LENGTH 2
const int pins[ARR_LENGTH] = {40, 50}; //pin of the slave select cable of the digital potentiometer [0,1...]

void setup() {
  for (size_t i = 0; i < ARR_LENGTH; i++) {
    pinMode(pins[i], OUTPUT);
  }
  pinMode(LED_BUILTIN, OUTPUT);

  SPI.begin();

  Serial.begin(4000000);
  Serial.println();
}

void loop() {
  // Recieve 6 bytes:
  // 0: channel (which drone)
  // 1: throttle
  // 2: pitch
  // 3: roll
  // 4: yaw
  // 5: '\0'
  if (Serial.available() > 5) {
    char in[6];
    Serial.readBytes(in, 6);// read 6bytes at once
    Serial.write(10);// comunicate to the host computer that the data was recived and read
    for (size_t i = 0; i < 4; i++) {// send values to the digital potentiometer
      digitalPotWrite(pins[in[0]], i, in[i + 1]);
      if (i == 0)
        analogWrite(LED_BUILTIN, in[1]);// visualize the throttle with the built in LED
    }
  }
}

void digitalPotWrite(int slaveSelect, int address, int value) {
  digitalWrite(slaveSelect, LOW);
  SPI.transfer(address);
  SPI.transfer(value);
  digitalWrite(slaveSelect, HIGH);
}
