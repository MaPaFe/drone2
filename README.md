# drone2

## The NEW and IMPROVED Zidröhnchen

Old drone: https://github.com/MaPaFe/Drone

### Compiling

```bash
mkdir build
cd build
cmake ..
make
```

### Running

1. Connect Arduino
2. Connect Kinect
3. Execute main program:

  ```bash
  ./build/drone
  ```
