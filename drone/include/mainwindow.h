#pragma once

#include "graphsarea.h"
#include "kinect.h"
#include "kinectarea.h"
#include "system.h"
#include "view3DArea.h"
#include <gtkmm.h>
#include <thread>

class MainWindow : public Gtk::Window {
public:
  MainWindow();
  virtual ~MainWindow();

private:
  void on_Button_connect_clicked();
  void on_Button_find_clicked();
  void on_Button_load_pids_clicked();
  void on_Button_update_pids_clicked();
  void on_Button_save_pids_clicked();
  void on_Scale_min_changed();
  void on_Scale_max_changed();
  void on_Scale_View3DArea_focal_changed();
  void on_Scale_View3DArea_x_rotation_changed();
  void on_Scale_View3DArea_y_rotation_changed();
  void on_Scale_View3DArea_z_rotation_changed();
  void on_hide() override;

  // Member data
  Gtk::Box m_Box_main;
  Gtk::AspectFrame m_AspectFrame;
  Gtk::ButtonBox m_Box_buttons;
  Gtk::Button m_Button_connect;
  Gtk::Button m_Button_find;
  Gtk::ButtonBox m_Box_scales;
  Gtk::Scale m_Scale_min;
  Gtk::Scale m_Scale_max;

  Gtk::Box m_pids_outside_Box;
  Gtk::ButtonBox m_BBox_manage_pids;
  Gtk::Button m_Button_load_pids;
  Gtk::Button m_Button_save_pids;
  Gtk::Button m_Button_update_pids;
  Gtk::Grid m_pids_outside_Grid;
  std::array<Gtk::Box, 9> m_pids_Box;
  std::array<Gtk::Label, 9> m_pids_Labels;
  std::array<Gtk::Entry, 9> m_pids_Entry;
  std::string m_pid_labels_data[9] = {"Kp    X", "Ki    X", "Kd    X",
                                      "Kp    Y", "Ki    Y", "Kd    Y",
                                      "Kp    Z", "Ki    Z", "Kd    Z"};

  System m_System;
  std::thread* m_System_Thread;
  KinectArea m_KinectArea;
  GraphsArea m_GraphsArea;
  View3DArea m_View3DArea;
  Gtk::Scale m_Scale_View3DArea_focal;
  Gtk::Scale m_Scale_View3DArea_x_rotation;
  Gtk::Scale m_Scale_View3DArea_y_rotation;
  Gtk::Scale m_Scale_View3DArea_z_rotation;
  //add_mark 	( 	double  	value,
  // PositionType  	position,
  // const Glib::ustring &  	markup
  // )
};
