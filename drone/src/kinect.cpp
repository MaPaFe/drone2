#include "kinect.h"
#include "kinectarea.h"
#include "system.h"
#include <array>
#include <iostream>

Kinect::Kinect()
    : m_shutdown(false),
      m_listener(libfreenect2::Frame::Depth) {

  m_device = nullptr;
  m_pipeline = nullptr;
  libfreenect2::setGlobalLogger(libfreenect2::createConsoleLogger(libfreenect2::Logger::Warning));
}

Kinect::~Kinect() {}

bool Kinect::connect() {
  if (m_freenect2.enumerateDevices() == 0) {
    std::cerr << "[Error] No devices available" << std::endl;
    return false;
  }

  m_pipeline = new libfreenect2::OpenCLKdePacketPipeline();
  m_device = m_freenect2.openDevice(m_freenect2.getDefaultDeviceSerialNumber(), m_pipeline);

  if (m_device == nullptr) {
    std::cerr << "[Error] Opening device" << std::endl;
    return false;
  }

  m_device->setIrAndDepthFrameListener(&m_listener);

  if (!m_device->startStreams(false, true)) {
    std::cerr << "[Error] Starting device" << std::endl;
    return false;
  }

  return true;
}

bool Kinect::main(System& system) {
  while (!m_shutdown) {
    if (!m_listener.waitForNewFrame(m_frames, 1000 * 10)) {
      std::cerr << "[Error] Recieving frames" << std::endl;
      return false;
    }
    libfreenect2::Frame* depth = m_frames[libfreenect2::Frame::Depth];
    // memcpy() copys byte for byte
    memcpy(system.m_depth, depth->data, 512 * 424 * 4);
    // copy data to drawArea's temporal array (4->size of float)
    system.new_frame(); // tell drawArea to update
    m_listener.release(m_frames);
  }

  m_device->stop();
  m_device->close();

  return true;
}

Sim::Vector Kinect::getPoint(const u_char* depth, int x, int y) const {
  std::cout << "getPoint depth              " << ((float*)depth)[512 * y + x] << std::endl;
  return Kinect::pixel_to_real(Sim::Vector(x, y, ((float*)depth)[512 * y + x]));
}

void Kinect::stop() { m_shutdown = true; }

// From https://openkinect.github.io/libfreenect2/classlibfreenect2_1_1Registration.html#a41a4d8479b97e4c74587ca431a94b2a3
Sim::Vector Kinect::pixel_to_real(Sim::Vector p) {
  // TODO use our own IrCameraParams
  // libfreenect2::Freenect2Device::IrCameraParams member

  // libfreenect2::Freenect2Device::IrCameraParams params(m_device->getIrCameraParams());
  // std::cout << params.fx << " " << params.fy << " " << params.cx << " " << params.cy << std::endl;
  // const float cx = params.cx, cy = params.cy;
  // const float fx = 1/params.fx, fy = 1/params.fy;

  const float cx = 256, cy = 212;
  const float fx = 364.18, fy = 364.33;
  Sim::Vector out;
  if (!std::isnan(p.z) && p.z > 0.0001f) {
    out.x = (p.x - cx) * p.z / fx;
    out.y = (p.y - cy) * p.z / fy;
    out.z = p.z;
  }
  return out;
}

Sim::Transform Kinect::pixel_to_real(Sim::Transform t) {
  // To measure: at distance ex. 1500, the drone is measured at 24 x-wide and 8 y-height at that distance. It follows that distance(1500) * width|height(24|8) / p.z the pespective corrects.
  // The focal lenght is canceled out (projection=(focal/p.z)*measured_width*(measured_depth/focal))
  const float fx = 364.18, fy = 364.33;
  Sim::Transform out;
  out.position = pixel_to_real(t.position);
  if (!std::isnan(out.position.z) && out.position.z > 0.0001f) {
    out.size.x = t.size.x * t.position.z / fx;
    out.size.y = t.size.y * t.position.z / fy;
  }
  return out;
}

Sim::Vector Kinect::real_to_pixel(Sim::Vector p) {
  const float cx = 256, cy = 212;
  const float fx = 364.18, fy = 364.33;
  Sim::Vector out;
  if (!std::isnan(p.z) && p.z > 0.0001f) {
    out.x = (p.x * fx / p.z) + cx;
    out.y = (p.y * fy / p.z) + cy;
    out.z = p.z;
  }
  return out;
}

Sim::Transform Kinect::real_to_pixel(Sim::Transform t) {
  const float fx = 364.18, fy = 364.33;
  Sim::Transform out;
  out.position = real_to_pixel(t.position);
  if (!std::isnan(out.position.z) && out.position.z > 0.0001f) {
    out.size.x = t.size.x * fx / t.position.z;
    out.size.y = t.size.y * fy / t.position.z;
  }
  return out;
}
