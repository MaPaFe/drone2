#include "system.h"
#include "controller.h"
#include "kinectarea.h"
#include <Simulator.h>
#include <array>
#include <chrono>
#include <cmath>
#include <fstream>
#include <iostream>
#include <opencv2/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <string>
#include <thread>

// System::System(KinectArea* d)
System::System(KinectArea* d, GraphsArea* p)
    : m_Kinect(),
      m_threshold_min(0.0),
      // for drone 2345.6
      // for puño 1000
      m_threshold_max(1607),
      p_drawer(d),
      p_plot(p),
      m_Communicator(),
      m_Controller(),
      pids(1),
      find_on(false) {

  m_depth = new unsigned char[512 * 424 * 4]; // Is equal to 4bytes*512

  m_Kinect_Thread = nullptr;

  //const Sim::Transform* t = new Sim::Transform();
  //m_drones.push_back(Sim::Drone(*t));
}

System::~System() {
  stop();
  delete[] m_depth;
}

bool System::connect() {
  // Create Kinect main thread
  if (m_Kinect_Thread == nullptr && m_Kinect.connect()) {
    m_Kinect_Thread = new std::thread([this] { m_Kinect.main(*this); });
    return true;
  }
  return false;
}

void System::stop() {
  // Magicly stops on its own, so this is not necessary, wtf...
  m_Kinect.stop();

  if (m_Kinect_Thread != nullptr && m_Kinect_Thread->joinable()) {
    m_Kinect_Thread->join();
    delete m_Kinect_Thread;
    m_Kinect_Thread = nullptr;
  }
}

// TODO fill todo.md with needed functions and classes
void System::main_loop() {
  while (true) {
    m_Controller.update();

    // std::cout << "isManual " << m_Controller.isManual
    //           << ", setpoint " << m_Controller.setpoint
    //           << ", throttle " << m_Controller.throttle
    //           << ", pitch " << m_Controller.pitch
    //           << ", roll " << m_Controller.roll
    //           << ", yaw " << m_Controller.yaw
    //           << std::endl;

    p_drawer->set_setpoint(Sim::Transform(m_Controller.setpoint, Sim::Vector(), Sim::Vector(85, 35)));

    if (m_Controller.isManual) {
      m_Communicator.send_payload(0, m_Controller.throttle,
                                     m_Controller.pitch,
                                     m_Controller.roll,
                                     m_Controller.yaw);
    } else
    // Calculate were the drones should be and update their last position
    {
      //Sim::predict(m_drones, Sim::Utils::time_now());
      std::lock_guard<std::mutex> guard(m_mutex_drones);
      for (size_t i = 0; i < m_drones.size(); i++) {
        Sim::Vector p = m_drones[i].calculate_pids(m_drones[i].get_last_transform(),
                                                   Sim::Transform(m_Controller.setpoint),
                                                   Sim::Utils::time_now());
        //std::cout << "pids" << p << std::endl;
        p_plot->add_pid(p);
        m_Communicator.send_payload(i, p);
      }
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(5));
  }
}

void System::find() {
  // Create a openCV matrix from the kinect depth
  for (int i = 0; i < 424 * 512; i++) {
    const float pixel_depth = ((float*)m_depth)[i];
    if (std::isnan(pixel_depth) || std::isinf(pixel_depth) || pixel_depth < 0.0) {
      ((float*)m_depth)[i] = 0;
    }
  }
  cv::Mat depth_mat(424, 512, CV_32FC1, m_depth);
  // std::cout << "depth_mat " << depth_mat.at<float>(424 / 2, 512 / 2) << std::endl;

  if (m_drones.size() > 0) {
    //std::cout << "m_drones.size()             " << m_drones.size() << std::endl;
    // Go through every Drone
    for (auto drone_it = m_drones.begin(); drone_it != m_drones.end();) {
      // TODO [moved to Sim] use history (velocity) to predict next position
      // TODO change box size based on velocity
      // PRIORITY if blob is on border, recalculate
      // TODO take into account aspect ratio /barra/ size
      Sim::Transform prediction = drone_it->get_last_transform();
      //Sim::Transform prediction = Sim::get_prediction(*drone_it, Sim::Utils::time_now());
      // Box search with the most recent transform
      Sim::Transform found = box_search(depth_mat, prediction);

      if (!found.position.is_any_nan()) {
        // Add it to it's history
        drone_it->update_transform(found);

        // For drawing a rect on the drone
        p_drawer->set_drone(found);
        // For drawing the graphs
        p_plot->add_position(found.position);
      }

      if (drone_it->is_position_lost(std::chrono::milliseconds(100))) {
        std::cout << "Deleting drone" << std::endl;
        drone_it = m_drones.erase(drone_it);
      } else {
        // Go to next Drone in the vector
        drone_it++;
      }
    }
  }
  // If there are no drones
  else {
    // Search from the center
    Sim::Transform found = box_search(depth_mat, Sim::Transform(m_Controller.setpoint));
    // If it found something
    if (!found.position.is_any_nan()) {
      std::cout << "!found.position.is_any_nan() true" << std::endl;
      // Create a Drone and store it
      m_drones.push_back(Sim::Drone(found));
      std::cout << "Added drone" << std::endl;
    }
  }
}

Sim::Transform System::box_search(cv::Mat& depth_mat, Sim::Transform prediction) const {
  std::cout << "box_search() prediction.pos " << prediction.position << std::endl;
  if (prediction.position.is_any_nan()) {
    std::cout << "prediction.pos.is_any_nan() " << prediction.position.is_any_nan() << std::endl;
    return Sim::Transform::NaN();
  }

  // Convert the real coordinates in pixels for openCV
  Sim::Vector pos = Kinect::real_to_pixel(prediction.position);
  std::cout << "pixels                      " << pos << std::endl;

  // Set the return Transform as NaN in case nothing is found
  Sim::Transform out;

  Sim::Vector size_vector = Kinect::real_to_pixel(prediction.size);
  const int size = !size_vector.is_all_nan() ? std::max(std::max(size_vector.x, size_vector.y), 2.0) : 10;
  for (int radius = size; radius < 100; radius += size) {
    // Define the start and end of the search box
    // TODO use real lengths instead of pixels
    const int start_x = Sim::Utils::constrain(pos.x - radius, 0, 512);
    const int end_x = Sim::Utils::constrain(pos.x + radius, 0, 512);
    const int start_y = Sim::Utils::constrain(pos.y - radius, 0, 424);
    const int end_y = Sim::Utils::constrain(pos.y + radius, 0, 424);
    // std::cout << "radius                      " << radius << std::endl;
    // std::cout << "x                           " << start_x << " " << end_x << std::endl;
    // std::cout << "y                           " << start_y << " " << end_y << std::endl;

    // Extract the box from the depth matrix
    cv::Mat search_box = depth_mat(cv::Range(start_y, end_y), cv::Range(start_x, end_x));
    // Keep only the parts inside the depth range
    cv::inRange(search_box, pos.z - radius * 5, pos.z + radius * 5, search_box);
    // Dilate and erode the Mat to remove unwanted artifacts
    // cv::dilate(search_box, search_box, cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3)), cv::Point(-1, -1), 3);
    // cv::erode(search_box, search_box, cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3)), cv::Point(-1, -1), 3);
    // Actually find the blobs in the box
    std::vector<std::vector<cv::Point>> contours;
    cv::findContours(search_box, contours, cv::RetrievalModes::RETR_LIST, cv::CHAIN_APPROX_SIMPLE);

    // std::cout << "contours.size()             " << contours.size() << std::endl;
    // cv::Mat show = cv::Mat::zeros(end_x - start_x, end_y - start_y, CV_8UC3);
    // cv::drawContours(show, contours, 0, cv::Scalar(255, 0, 0));

    float average_depth = 0;
    int pixel_count = 0;
    for (int i = 0; i < search_box.rows; i++) {
      const bool* row = search_box.ptr<bool>(i);
      for (int j = 0; j < search_box.cols; j++) {
        if (row[j]) {
          average_depth += ((float*)m_depth)[512 * (start_y + i) + (start_x + j)];
          pixel_count++;
        }
      }
    }
    const float depth = average_depth / pixel_count;
    std::cout << "average_depth               " << depth << " " << pixel_count << std::endl;

    if (contours.size() > 0) {
      // Create a rect to get theinates of it
      cv::Rect rect = cv::boundingRect(contours[0]);
      std::cout << "rect: x                     " << start_x + rect.x << std::endl;
      std::cout << "rect: y                     " << start_y + rect.y << std::endl;

      // out.position = m_Kinect.getPointth,
      //                                  start_x + rect.x + (rect.width / 2),
      //                                  start_y + rect.y + (rect.height / 2));
      // Update the return Sim::Transform with the real coordinates of the
      out = Kinect::pixel_to_real(Sim::Transform(Sim::Vector(start_x + rect.x + (rect.width / 2),
                                                             start_y + rect.y + (rect.height / 2),
                                                             depth),
                                                 Sim::Vector(),
                                                 Sim::Vector(rect.width, rect.height)));
      std::cout << "box_search() out            " << out.position << std::endl;
      std::cout << out << std::endl;
      p_drawer->set_drone(out);
      return out;
    }
  }
  std::cout << "box_search not found" << std::endl;
  return out;
}

bool System::toggle_find() {
  if (find_on) {
    find_on = false;
  } else {
    find_on = true;
  }
  return find_on;
}

void System::new_frame() {
  if (find_on)
    find();
  if (p_drawer != nullptr)
    p_drawer->queue_draw();
}

void System::load_pids(int channel) {
  try {
    std::ifstream infile;
    infile.exceptions(std::ifstream::eofbit | std::ifstream::failbit | std::ifstream::badbit);
    infile.open("PID_" + std::to_string(channel) + ".save", std::fstream::binary);
    infile.read((char*)pids[channel].data(), 9 * 8);
    infile.close();
    {
      std::lock_guard<std::mutex> guard(m_mutex_drones);
      m_drones[channel].update_pids(pids[channel]);
    }
  } catch (std::exception const& e) {
    std::cerr << "There was an error loading pids, does the file PID_" + std::to_string(channel) + ".save" + " exist?, (the channel is " + std::to_string(channel) + "): " << e.what() << std::endl;
  }
}

void System::save_pids(int channel) {
  try {
    std::ofstream outfile;
    outfile.exceptions(std::ofstream::eofbit | std::ofstream::failbit | std::ofstream::badbit);
    outfile.open("PID_" + std::to_string(channel) + ".save", std::fstream::binary);
    outfile.write((const char*)pids[channel].data(), 9 * 8); //sizeof(double)-->8
    outfile.close();
  } catch (std::exception const& e) {
    std::cerr << "There was an error saving pids, does the file PID_" + std::to_string(channel) + ".save" + " exist?, (the channel is " + std::to_string(channel) + "): " << e.what() << std::endl;
  }
}

void System::update_pids(int channel, std::array<double, 9> k) {
  std::ofstream outfile;
  outfile.open("PID_" + std::to_string(channel) + ".tmp", std::fstream::binary);
  outfile.write((char*)pids[channel].data(), 9 * 8);
  outfile.close();
  pids[channel] = k;
  if (!m_drones.empty()) {
    std::lock_guard<std::mutex> guard(m_mutex_drones);
    m_drones[channel].update_pids(k);
  }
}
