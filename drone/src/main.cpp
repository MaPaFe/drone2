#include "mainwindow.h"
#include <gtkmm/application.h>
#include <gtkmm/window.h>
#include <signal.h>

void signalHandler(int s) { exit(s); }

int main(int argc, char* argv[]) {
  signal(SIGINT, signalHandler);

  auto app = Gtk::Application::create(argc, argv, "org.mapafe.drone");
  MainWindow window;

  return app->run(window);
}
