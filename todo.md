# Simulator feature list

## find

- [ ] 3D blobs

## Drone

- [ ] config
- [ ] weight
- [ ] internal pids (the not known)
- [ ] motor force etc.
- [x] transform history in a std::vector
- [x] calculate_pids()
- [x] pids array

## utils

- [x] vector
- [x] Transform
  - [x] pos
  - [x] rot
  - [x] size

## pids

- [x] set_k()
- [x] calculate(value)
- [x] like processing

## Simulator

- [x] predict(drone, time) -> were is it at time
- [ ] when update from Kinect, update history with more accurate positions with magic math (Verlet correction?)
  - [ ] temporary: simple map
  - [ ] use curve fitting ?
- [ ] use PID outputs to predict better

## simtest

- [ ] test different prediction algorithms
  - [ ] shitty interpolation correction
  - [ ] recalculate verlet with new point (reverse?)
  - [ ] different amount of invented points
  - [ ] only real points
  - [ ] 
