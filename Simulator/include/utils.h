#pragma once

#include <chrono>
#include <cmath>
#include <iostream>
#include <limits>

#define QUIET_NAN std::numeric_limits<double>::quiet_NaN()
typedef std::chrono::high_resolution_clock::time_point time_point;
// #define TIME_NOW std::chrono::high_resolution_clock::now()

namespace Sim {

namespace Utils {

time_point time_now();

std::chrono::microseconds diff_time(std::chrono::high_resolution_clock::time_point left, std::chrono::high_resolution_clock::time_point rigth);

int constrain(int val, int min, int max);

float map(float val, float in_min, float in_max, float out_min, float out_max);

} // namespace Utils

struct Vector {
  explicit Vector(double a = QUIET_NAN, double b = QUIET_NAN, double c = QUIET_NAN)
      : x(a), y(b), z(c){};

  double x, y, z;

  inline bool is_any_nan() const {
    return std::isnan(x) || std::isnan(y) || std::isnan(z);
  };

  inline bool is_all_nan() const {
    return std::isnan(x) && std::isnan(y) && std::isnan(z);
  };

  inline bool operator==(const Vector& other) const {
    return (other.x == x || std::isnan(other.x) == std::isnan(x)) &&
           (other.y == y || std::isnan(other.y) == std::isnan(y)) &&
           (other.z == z || std::isnan(other.z) == std::isnan(z));
  };

  inline bool operator!=(const Vector& other) const {
    return !(other == *this);
  };

  inline double operator[](size_t& index) const {
    if (index == 0)
      return x;
    if (index == 1)
      return y;
    if (index == 2)
      return z;
    throw "Maximum index 2";
  };

  inline double& operator[](size_t& index) {
    if (index == 0)
      return x;
    if (index == 1)
      return y;
    if (index == 2)
      return z;
    throw "Maximum index 2";
  };

  inline Vector operator+(const Vector& other) const {
    return Vector(x + other.x,
                  y + other.y,
                  z + other.z);
  }

  inline Vector operator-(const Vector& other) const {
    return Vector(x - other.x,
                  y - other.y,
                  z - other.z);
  }

  inline Vector operator*(double other) const {
    return Vector(x * other,
                  y * other,
                  z * other);
  }

  inline Vector operator/(double other) const {
    return Vector(x / other,
                  y / other,
                  z / other);
  }

  inline friend std::ostream& operator<<(std::ostream& out, const Vector& vector) {
    return out << vector.x << ' ' << vector.y << ' ' << vector.z;
  }

  static Vector NaN() { return Vector(); };
};

struct Transform {
  explicit Transform(Vector p = Vector(), Vector r = Vector(), Vector s = Vector(), time_point t = Sim::Utils::time_now())
      : position(p), rotation(r), size(s), time(t){};

  Vector position, rotation, size;
  time_point time;

  inline bool is_any_nan() const {
    return position.is_any_nan() || rotation.is_any_nan() || size.is_any_nan();
  };

  inline bool operator==(const Transform& other) const {
    return other.position == position && other.rotation == rotation && other.size == size;
  };

  inline bool operator!=(const Transform& other) const {
    return !(other == *this);
  };

  inline Vector operator[](size_t& index) const {
    if (index == 0)
      return position;
    if (index == 1)
      return rotation;
    if (index == 2)
      return size;
    throw "Maximum index 2";
  };

  inline Vector& operator[](size_t& index) {
    if (index == 0)
      return position;
    if (index == 1)
      return rotation;
    if (index == 2)
      return size;
    throw "Maximum index 2";
  };

  inline friend std::ostream& operator<<(std::ostream& out, const Transform& p) {
    return out << p.position << std::endl
               << p.rotation << std::endl
               << p.size << std::endl
               << p.time.time_since_epoch().count();
  }

  static Transform NaN() { return Transform(); };
};

} // namespace Sim
