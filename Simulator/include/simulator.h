#pragma once

#include "drone.h"
#include "utils.h"
#include <chrono>
#include <vector>

namespace Sim {
Sim::Transform get_prediction(Sim::Drone& drone, time_point time);
Sim::Transform get_prediction_verlet(Sim::Drone& drone, time_point time);
Sim::Transform get_prediction_verlet_parabola(Sim::Drone& drone, time_point time);
void correct(std::vector<Drone>& drones, time_point time);
void predict(std::vector<Drone>& drones, time_point time);
} // namespace Sim
