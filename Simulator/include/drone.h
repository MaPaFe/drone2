#pragma once

#include "pid.h"
#include "utils.h"
#include <array>
#include <chrono>
#include <deque>

namespace Sim {
class Drone {
public:
  explicit Drone(const Transform& t);
  virtual ~Drone();

  void update_transform(Transform t);
  Sim::Vector calculate_pids(Transform input, Transform setpoint, time_point time);
  const std::deque<Transform>& get_transform_history() const;
  const Transform& get_last_transform() const;
  bool is_nan() const;
  bool is_position_lost(std::chrono::microseconds time) const;
  void update_pids(std::array<double, 9> k);

private:
  int max_history_size;
  std::array<Pid, 3> pids;
  std::deque<Transform> m_transform_history;
};
} // namespace Sim
