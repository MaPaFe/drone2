#pragma once

#include "utils.h"

namespace Sim {
class Pid {
public:
  Pid();
  virtual ~Pid();

  void set_k(double p, double i, double d);
  void set_min_max_out(double min, double max);
  double calculate(double input, double setpoint, time_point time);

private:
  double k_p, k_i, k_d;
  double min_output, max_output;
  double previous_error, integral;
  time_point last_time;
};
} // namespace Sim
