#include "pid.h"
#include "utils.h"
#include <chrono>

Sim::Pid::Pid() : k_p(0), k_i(0), k_d(0) {
  set_min_max_out(-127, 127);

  previous_error = 0;
  integral = 0;
}

Sim::Pid::~Pid() {}

void Sim::Pid::set_k(double p, double i, double d) {
  k_p = p;
  k_i = i;
  k_d = d;
}

void Sim::Pid::set_min_max_out(double min, double max) {
  min_output = min;
  max_output = max;
}

double Sim::Pid::calculate(double input, double setpoint, time_point time) {
  const double delta_time = Sim::Utils::diff_time(time, last_time).count();

  const double error = setpoint - input;

  integral += error * delta_time;
  //integral = constrain(integral, ?, ?);

  //                    proportional  integral------   derivative-----------------------------------
  const double output = error * k_p + integral * k_i + ((error - previous_error) / delta_time) * k_d;

  previous_error = error;
  last_time = time;

  return Sim::Utils::constrain(output, min_output, max_output);
}
