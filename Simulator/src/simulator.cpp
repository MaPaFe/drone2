#include "simulator.h"
#include <chrono>

// TODO simple map
void Sim::correct(std::vector<Sim::Drone>& drones, time_point time) {}

Sim::Transform Sim::get_prediction(Sim::Drone& drone, time_point time) {
  const std::deque<Sim::Transform> history = drone.get_transform_history();
  const Sim::Transform last_transform = drone.get_last_transform();
  Sim::Transform out = last_transform;
  out.time = time;

  if (history[1].position.is_all_nan())
    return out;

  const auto vel_time_diff = Sim::Utils::diff_time(last_transform.time, history[1].time);
  const auto out_time_diff = Sim::Utils::diff_time(time, last_transform.time);

  const Sim::Vector velocity = (last_transform.position - history[1].position) / vel_time_diff.count();

  out.position = last_transform.position + (velocity * out_time_diff.count());
  return out;
}

Sim::Transform Sim::get_prediction_verlet(Sim::Drone& drone, time_point time) {
  const std::deque<Sim::Transform> history = drone.get_transform_history();
  Sim::Transform out = drone.get_last_transform();
  out.time = time;

  if (history[1].position.is_all_nan())
    return out;

  Sim::Vector xi = drone.get_last_transform().position;
  Sim::Vector xi_1 = history[1].position;
  Sim::Vector xi_2 = history[2].position;

  int64_t dti = Sim::Utils::diff_time(time, drone.get_last_transform().time).count();
  int64_t dti_1 = Sim::Utils::diff_time(history[1].time, history[2].time).count();

  Sim::Vector a = ((((xi-xi_1)/(dti))  -  ((xi_1-xi_2)/(dti_1))) /(dti))/1;

  out.position =  xi + (xi - xi_1) * (dti / dti_1) + a * dti * dti;
  return out;
}

Sim::Transform Sim::get_prediction_verlet_parabola(Sim::Drone& drone, time_point time) {
  const std::deque<Sim::Transform> history = drone.get_transform_history();
  Sim::Transform out = drone.get_last_transform();
  out.time = time;

  if (history[1].position.is_all_nan())
    return out;

  Sim::Vector xi = drone.get_last_transform().position;
  Sim::Vector xi_1 = history[1].position;
  Sim::Vector xi_2 = history[2].position;

  int64_t dti = Sim::Utils::diff_time(time, drone.get_last_transform().time).count();
  int64_t dti_1 = Sim::Utils::diff_time(history[1].time, history[2].time).count();

  // / / s = 0,5 · a · t2 + vo · t + s0
  //   a = 2*(s-v0*t-s0)/(t*t)
  //   t0 = xi_1.time
  //   t = xi.time
  //   v0 = (x_2-xi_1)/(dti_2)
  //   s0 = xi_1
  //   s = xi
  //   a = 2*(s-v0*t-s0)/(t*t)
    
  //   a = 2*(xi-(x_2-xi_1)/(dti_2)*xi.time-xi_1)/(xi.time*xi.time)
  // / /
  Sim::Vector a;// = //a = 2*(drone.get_last_transform().position/t^2);//((((xi-xi_1)/(dti))  -  ((xi_1-xi_2)/(dti_1))) /(dti))/1;

  out.position =  xi + (xi - xi_1) * (dti / dti_1) + a * dti * dti;
  return out;
}

void Sim::predict(std::vector<Sim::Drone>& drones, time_point time) {
  for (Drone& d : drones)
    d.update_transform(get_prediction(d, time));
}
