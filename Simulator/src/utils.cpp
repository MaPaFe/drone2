#include "utils.h"

std::chrono::high_resolution_clock::time_point Sim::Utils::time_now() {
  return std::chrono::high_resolution_clock::now();
}

std::chrono::microseconds Sim::Utils::diff_time(std::chrono::high_resolution_clock::time_point left, std::chrono::high_resolution_clock::time_point right) {
  return std::chrono::duration_cast<std::chrono::microseconds>(left - right);
}

int Sim::Utils::constrain(int val, int min, int max) {
  return val < min ? min : max < val ? max : val;
}

float Sim::Utils::map(float val, float in_min, float in_max, float out_min, float out_max) {
  return (val - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}
