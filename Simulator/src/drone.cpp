#include "drone.h"
#include "utils.h"
#include <deque>

Sim::Drone::Drone(const Sim::Transform& t)
    : max_history_size(10),
      m_transform_history(max_history_size),
      pids() {
  update_transform(t);

  pids[0].set_k(1, 0, 1); // Y Throttle
  pids[1].set_k(1, 0, 1); // Z Pitch
  pids[2].set_k(1, 0, 1); // X Roll
}

Sim::Drone::~Drone() {}

void Sim::Drone::update_transform(Sim::Transform t) {
  std::cout << m_transform_history.size() << std::endl;
  // Bug siempre vacío (comprobar)
  while (m_transform_history.size() >= max_history_size)
    m_transform_history.pop_back();
  m_transform_history.push_front(t);
}

Sim::Vector Sim::Drone::calculate_pids(Sim::Transform input, Sim::Transform setpoint, time_point time) {
  Sim::Vector out;
  for (size_t i = 0; i < 3; i++)
    out[i] = pids[i].calculate(input.position[i], setpoint.position[i], time);
  return out;
}

const std::deque<Sim::Transform>& Sim::Drone::get_transform_history() const {
  return m_transform_history;
}

const Sim::Transform& Sim::Drone::get_last_transform() const {
  return m_transform_history.front();
}

bool Sim::Drone::is_position_lost(std::chrono::microseconds time) const {
  return Sim::Utils::diff_time(Sim::Utils::time_now(), get_last_transform().time) > time;
}

void Sim::Drone::update_pids(std::array<double, 9> k) {
  //TODO correct if necesary coordinate to drone movement
  pids[0].set_k(k[0], k[1], k[2]); // Y Throttle
  pids[1].set_k(k[3], k[4], k[5]); // Z Pitch
  pids[2].set_k(k[6], k[7], k[8]); // X Roll
}
